/**
 *
 * @license KOrgParser - a library to parse a org-file (emacs) file into a tree structure or create an org-file
 * Copyright (C) {2017}  eduardoml

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */


abstract class OrgComponent(c : String = "", l : Int = 0) {

    private var m_components :MutableList<OrgComponent> = arrayListOf()
    private var m_properties : MutableList<OrgProperty> = arrayListOf()
    private var m_content : String = c
    private var m_level : Int = l

    abstract fun string() : String

    fun addOrgComponent( c : OrgComponent) {
        m_components.add(c)
    }

    fun addOrgProperty( p : OrgProperty) {
        m_properties.add(p)
    }

    fun setContent(lines : String) {
        m_content = lines
    }

    fun level() : Int{
        return m_level
    }

    fun content() : String {
        return m_content
    }

    //fun getComponent(line : String) : OrgComponent {
   // }

}