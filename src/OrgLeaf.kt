/**
 *
 * @license KOrgParser - a library to parse a org-file (emacs) file into a tree structure or create an org-file
 * Copyright (C) {2017}  eduardoml

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * Represents a (bulleted) item in the org file
 * @property lines contains the title and the content of a given item. If there are subitens, they are here as well
 * @property level of the leaf (roughly how many *'s there are)
 * @property leafTitle is the * leafTitle .... \n
 * @property leafStatus is the all capitalized word following the *'s can be TODO, DONE or anything else
  */
data class OrgLeaf(var lines : String = "", var level : Int = 0) : OrgComponent(lines, level) {
    var leafTitle: String = ""
    var leafStatus: String = ""
    // TODO esses parsers poderiam muito bem ser funcoes estaticas
    var propParser = OrgPropertyParser()
    var statParser = OrgStatusParser()
    var leafParser = OrgLeafParser()

    init {
        // assemble the regex pattern to search for subitens
        val subLeafs = leafParser.parse(lines, this)
        propParser.parse(lines, this)
        statParser.parse(lines, this)
    }

    fun setStatus(s : String) {
        leafStatus = s
    }

    override fun string(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}