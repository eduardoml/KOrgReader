/**
 *
 * @license KOrgParser - a library to parse a org-file (emacs) file into a tree structure or create an org-file
 * Copyright (C) {2017}  eduardoml

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

import java.io.BufferedReader
import java.io.File

fun main( args : Array<String>) {

    val fName : String =  "milestones.org"
    var file: BufferedReader = File(fName).bufferedReader()
    var content : String = file.use({it.readText()})
    /*var content : MutableList<String> = arrayListOf()
    file.useLines { line -> line.forEach { content.add(it) } }

    for (line in content){
        println((line.substring(0, 10).count({ c: Char -> c == '*'})).toString() + " ====> " + line)
    }*/
    /*val reg : Regex = "(?<=[\n* ])(?=[\n* ])".toRegex()*/
    var leaf : OrgLeaf = OrgLeaf(content, level = 0)
}